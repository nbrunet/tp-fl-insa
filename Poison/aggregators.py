import numpy as np
def aggregator_mean(weights_list):
    return np.mean(weights_list,axis=0)

def aggregator_custom(weights_list):
    '''
    :param weights_list:
    :return: np.array(_______ ,dtype='object')

    Must return the same objects as aggregator_mean and aggregator_median
    '''
    return None